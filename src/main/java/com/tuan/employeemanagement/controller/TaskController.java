package com.tuan.employeemanagement.controller;

import com.tuan.employeemanagement.exception.ResourceNotFoundException;
import com.tuan.employeemanagement.model.Employees;
import com.tuan.employeemanagement.model.Task;
import com.tuan.employeemanagement.repository.TaskReponsitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.tuan.employeemanagement.utilities.isValiDate.isValidDate;

@RestController
@RequestMapping("/api/v1/")
public class TaskController {
    @Autowired
    private TaskReponsitory taskReponsitory;
    @GetMapping("task")
    public List<Task> getAllTask(){

        return this.taskReponsitory.findAll();
    }
    //get employees by id
    @GetMapping("task/{id}")
    public ResponseEntity<Task> getTaskById(@PathVariable(value = "id")Long taskId)
            throws ResourceNotFoundException {
        Task task= taskReponsitory.findById(taskId).orElseThrow(()-> new ResourceNotFoundException("Employee not found for this id :: "+taskId));
        return ResponseEntity.ok().body(task);
    }
    //create employees
    @PostMapping("task")
    public  Task createTask(@RequestBody Task task) throws ResourceNotFoundException{
        if(task.getTaskName().isEmpty() ||
                task.getDecription().isEmpty() ||

        task.getStatus().isEmpty()|| task.getDeadline().isEmpty()){
            throw new ResourceNotFoundException("Please enter field!!! ");
        }
        if(task.getStatus().contains("0")){
            task.setStatus("Todo");
        }else if(task.getStatus().contains("1")){
            task.setStatus("In Progress");
        }else
        if(task.getStatus().contains("2")){
            task.setStatus("done");
        }
        if(isValidDate(task.getDeadline(),"dd-MM-yyyy HH:mm")== true){
            task.setDeadline(task.getDeadline());
        }else {
            throw new ResourceNotFoundException("incorrect!!! please follow the pattern dd-mm-yyyy HH:mm");
        }

        return this.taskReponsitory.save(task);

    }
//    public static boolean isValidDate(String dateString) {
//        boolean isvalid;
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
//        try{
//           formatter.parse(dateString);
//           isvalid = true;
//
//        }catch (Exception e){
//            isvalid = false;
//
//        }
//        return isvalid;
//
//    }
    //update employees
    @PutMapping("task/{id}")
    public ResponseEntity<Task> updateTask(@PathVariable(value = "id") Long taskId, @Validated @RequestBody Task taskDetail)
            throws ResourceNotFoundException{

        Task task = taskReponsitory.findById(taskId)
                .orElseThrow(() -> new ResourceNotFoundException("task not found for this id:: "+taskId));
        if(task.getTaskName().isEmpty() ||
                task.getDecription().isEmpty() ||

                task.getStatus().isEmpty()|| task.getDeadline().isEmpty()){
            throw new ResourceNotFoundException("Please enter field!!! ");
        }
        if(taskDetail.getStatus().contains("0")){
            task.setStatus("Todo");
        }else if(taskDetail.getStatus().contains("1")){
            task.setStatus("In Progress");
        }else
        if(taskDetail.getStatus().contains("2")){
            task.setStatus("done");
        }
        if(isValidDate(taskDetail.getDeadline(),"dd-MM-yyyy HH:mm")== true){
            task.setDeadline(taskDetail.getDeadline());
        }else {
            throw new ResourceNotFoundException("incorrect!!! please follow the pattern dd-mm-yyyy HH:mm");
        }
        task.setTaskName(taskDetail.getTaskName());
        task.setDecription(taskDetail.getDecription());
        task.setAssignee(taskDetail.getAssignee());
        //task.setDeadline(taskDetail.getDeadline());

        //task.setStatus(taskDetail.getStatus());

        return ResponseEntity.ok(this.taskReponsitory.save(task));

    }
    //delete employees
    @DeleteMapping("task/{id}")
    public Map<String, Boolean> deleteTask(@PathVariable(value = "id") Long taskId)
            throws ResourceNotFoundException{
        Task task = taskReponsitory.findById(taskId)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id:: "+taskId));

        this.taskReponsitory.delete(task);

        Map<String,Boolean> response = new HashMap<>();
        response.put("delete", Boolean.TRUE);

        return response;
    }
}
